package com.example.companymeetingscheduler;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.companymeetingscheduler.Utils.Constants;
import com.example.companymeetingscheduler.Utils.Dialogues;
import com.example.companymeetingscheduler.Utils.UtilityMethods;
import com.example.companymeetingscheduler.adapter.AppointListAdapter;
import com.example.companymeetingscheduler.callbacks.CallBackToView;
import com.example.companymeetingscheduler.model.Appointment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private UtilityMethods utilityMethods;
    private ArrayList<Appointment> appointmentsList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;
    private Calendar dateSelected;
    private LinearLayout llPrev, llNext;
    private TextView tvDate;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {

            if (UtilityMethods.isNetworkConnected(this)) {
                initializeView();
                proceed();

            } else {
                Dialogues dialogues = new Dialogues(this);
                dialogues.noInternetConnection(new CallBackToView() {
                    @Override
                    public void onSuccess() {
                        if (UtilityMethods.isNetworkConnected(MainActivity.this)) {
                            initializeView();
                            proceed();
                        } else {
                            Toast.makeText(MainActivity.this, Constants.MESSAGE_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }

                    @Override
                    public void onFailure() {
                        Toast.makeText(MainActivity.this, Constants.MESSAGE_NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
            }

        }
    }

    private void initializeView() {
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view2);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        utilityMethods = new UtilityMethods(this);
        tvDate = findViewById(R.id.tv_date);
        llNext = findViewById(R.id.ll_next);
        llPrev = findViewById(R.id.ll_prev);
        button = findViewById(R.id.btn_schedule_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ScheduleMeetingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.INTENT_APPOINTMENT, (Serializable) appointmentsList);
                bundle.putSerializable(Constants.BUNDLE_SELECTED_DATE, dateSelected);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }
        });

        llPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dateSelected.add(Calendar.DAY_OF_MONTH, -1);
                setSelectedDateTitle();
                checkDate();
                getMeetingsData();


            }
        });

        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSelected.add(Calendar.DAY_OF_MONTH, +1);
                setSelectedDateTitle();
                checkDate();
                getMeetingsData();

            }
        });

    }

    public void proceed() {
        dateSelected = Calendar.getInstance();
        dateSelected.setTime(new Date());
        setSelectedDateTitle();
        getMeetingsData();
    }

    private void setSelectedDateTitle() {
        SimpleDateFormat formatter;
        if (UtilityMethods.getScreenOrientation(this).equals("SCREEN_ORIENTATION_PORTRAIT") || UtilityMethods.getScreenOrientation(this).equals("SCREEN_ORIENTATION_REVERSE_PORTRAIT")) {
            formatter = new SimpleDateFormat(Constants.DATE_MEETING_FORMAT);
        } else {
            formatter = new SimpleDateFormat(Constants.DATE_MEETING_FORMAT_LANDSCAPE);
        }
        String formattedDate = formatter.format(dateSelected.getTime());
        tvDate.setText(formattedDate);
    }
    
    private void checkDate() {
        button.setEnabled(true);
        button.setBackground(getResources().getDrawable(R.drawable.rounded_shape));


        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(new Date());
        currentDate.clear(Calendar.SECOND);
        currentDate.clear(Calendar.MILLISECOND);
        currentDate.clear(Calendar.MINUTE);
        currentDate.clear(Calendar.HOUR_OF_DAY);
        currentDate.clear(Calendar.HOUR);
        currentDate.set(Calendar.HOUR_OF_DAY, 0);

        dateSelected.clear(Calendar.SECOND);
        dateSelected.clear(Calendar.MILLISECOND);
        dateSelected.clear(Calendar.MINUTE);
        dateSelected.clear(Calendar.HOUR_OF_DAY);
        dateSelected.clear(Calendar.HOUR);
        dateSelected.set(Calendar.HOUR_OF_DAY, 0);

        if (dateSelected.compareTo(currentDate) < 0) {
            button.setEnabled(false);
            button.setBackground(getResources().getDrawable(R.drawable.rounded_shape_disable));
        }
    }

    public void getMeetingsData() {
        utilityMethods.showLoader(Constants.MESSAGE_FETCHING_MEETINGS);
        RequestQueue queue = Volley.newRequestQueue(this);

        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_API_FORMAT);
        String date = formatter.format(dateSelected.getTime());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.API_URL + date,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray obj = new JSONArray(response);
                            setDataToView(obj);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        utilityMethods.hideLoader();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                utilityMethods.hideLoader();
            }
        });

        queue.add(stringRequest);


    }

    private void setDataToView(JSONArray obj) {


        appointmentsList = new ArrayList<Appointment>();

        for (int i = 0; i < obj.length(); i++) {
            try {
                JSONObject object = (JSONObject) obj.get(i);
                Appointment appointment = new Appointment();
                appointment.setDescription(object.getString(Constants.APPOINTMENT_DESCRIPTION));


                JSONArray jsonArray = (JSONArray) object.get(Constants.APPOINTMENT_PARTICIPANTS);
                ArrayList<String> participantsList = new ArrayList<String>();
                for (int j = 0; j < jsonArray.length(); j++) {
                    participantsList.add((String) jsonArray.get(j));
                }

                appointment.setParticipants(participantsList);


                Calendar startCal = Calendar.getInstance();
                startCal.setTime(dateSelected.getTime());
                startCal.clear(Calendar.MINUTE);
                startCal.clear(Calendar.SECOND);
                startCal.clear(Calendar.MILLISECOND);
                startCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(object.getString(Constants.APPOINTMENT_START_TIME).split(":")[0]));
                startCal.set(Calendar.MINUTE, Integer.parseInt(object.getString(Constants.APPOINTMENT_START_TIME).split(":")[1]));

                Calendar endCal = Calendar.getInstance();
                endCal.setTime(dateSelected.getTime());
                endCal.clear(Calendar.MINUTE);
                endCal.clear(Calendar.SECOND);
                endCal.clear(Calendar.MILLISECOND);
                endCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(object.getString(Constants.APPOINTMENT_END_TIME).split(":")[0]));
                endCal.set(Calendar.MINUTE, Integer.parseInt(object.getString(Constants.APPOINTMENT_END_TIME).split(":")[1]));

                appointment.setStart_time(startCal.getTimeInMillis());
                appointment.setEnd_time(endCal.getTimeInMillis());
                appointmentsList.add(appointment);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        sortAppointmentList();
        mAdapter = new AppointListAdapter(appointmentsList);
        recyclerView.setAdapter(mAdapter);
    }

    private void sortAppointmentList() {
        Collections.sort(appointmentsList, new Comparator<Appointment>() {
            @Override
            public int compare(Appointment appointment, Appointment t1) {
                Date date1 = new Date(appointment.getStart_time());
                Date date2 = new Date(t1.getStart_time());
                return date1.compareTo(date2);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Dialogues dialogues = new Dialogues(this);
        dialogues.exitApp(new CallBackToView() {
            @Override
            public void onSuccess() {
                finish();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(appointmentsList != null && appointmentsList.size()>0){
            outState.putSerializable(Constants.INTENT_APPOINTMENT, (Serializable) appointmentsList);
        }
        if(dateSelected != null){
            outState.putSerializable(Constants.BUNDLE_SELECTED_DATE, dateSelected);
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        initializeView();
        appointmentsList = (ArrayList<Appointment>) savedInstanceState.getSerializable(Constants.INTENT_APPOINTMENT);
        dateSelected = (Calendar) savedInstanceState.getSerializable(Constants.BUNDLE_SELECTED_DATE);
        setSelectedDateTitle();
        if(appointmentsList != null && appointmentsList.size()>0){
            sortAppointmentList();
            mAdapter = new AppointListAdapter(appointmentsList);
            recyclerView.setAdapter(mAdapter);

        }
    }
}
