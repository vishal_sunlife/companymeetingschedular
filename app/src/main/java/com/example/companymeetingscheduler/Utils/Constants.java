package com.example.companymeetingscheduler.Utils;

public class Constants {
    public static String SCHEDULE_A_MEETING = "SCHEDULE A MEETING";
    public static String BUNDLE = "Bundle";
    public static String BUNDLE_SELECTED_DATE = "SelectedDate";
    public static String INTENT_APPOINTMENT = "Appointment";
    public static String DATE_DEFAULT_FORMAT = "EEE MMM dd HH:mm:ss z yyyy";
    public static String DATE_MEETING_FORMAT = "dd-MM-yyyy";
    public static String DATE_MEETING_FORMAT_LANDSCAPE = "EEEE, dd-MM-yyyy";
    public static String DATE_API_FORMAT = "dd/MM/yyyy";
    public static String MESSAGE_SELECT_START_END_TIME = "Please select start time and end time";
    public static String WRONG_TIME = "End time sould be after start time";
    public static String SLOT_AVAILABLE = "Slot Available";
    public static String SLOT__NOT_AVAILABLE = "Slot Not Available";
    public static String MESSAGE_NO_INTERNET_CONNECTION = "No Internet Connection";
    public static String TURN_ON_INTERNET = "Please check your internet connection and turn it on";
    public static String EXIT_TITLE = "Exit";
    public static String EXIT_MESSAGE = "Do you want to exit?";
    public static String API_URL = "http://fathomless-shelf-5846.herokuapp.com/api/schedule?date=";
    public static String MESSAGE_FETCHING_MEETINGS = "Fetching meetings";
    public static String APPOINTMENT_DESCRIPTION = "description";
    public static String APPOINTMENT_PARTICIPANTS = "participants";
    public static String APPOINTMENT_START_TIME = "start_time";
    public static String APPOINTMENT_END_TIME = "end_time";




}
