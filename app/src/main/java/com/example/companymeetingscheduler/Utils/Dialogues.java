package com.example.companymeetingscheduler.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.example.companymeetingscheduler.callbacks.CallBackToView;

public class Dialogues {

    private Context context;
    public Dialogues(Context context) {
        this.context = context;
    }

    public void noInternetConnection(final CallBackToView callBackToView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Constants.MESSAGE_NO_INTERNET_CONNECTION);
        builder.setMessage(Constants.TURN_ON_INTERNET);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callBackToView.onSuccess();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callBackToView.onFailure();
            }
        });

        builder.show();
    }

    public void exitApp(final CallBackToView callBackToView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Constants.EXIT_TITLE);
        builder.setMessage(Constants.EXIT_MESSAGE);
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callBackToView.onSuccess();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callBackToView.onFailure();
            }
        });

        builder.show();
    }

}
