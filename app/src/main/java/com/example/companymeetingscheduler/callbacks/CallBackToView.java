package com.example.companymeetingscheduler.callbacks;

public interface CallBackToView {

    void onSuccess();

    void onFailure();
}
