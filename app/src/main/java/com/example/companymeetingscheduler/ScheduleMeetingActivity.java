package com.example.companymeetingscheduler;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.companymeetingscheduler.Utils.Constants;
import com.example.companymeetingscheduler.model.Appointment;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScheduleMeetingActivity extends AppCompatActivity {

    private List<Appointment> appointmentList;
    private TextView tvDate, tvStartTime, tvEndTime;
    private Calendar selectedDate;
    private String selectedDateString, selectedStartTimeString, selectedEndTimeString;
    private Calendar selectedStartTime;
    private Calendar selectedEndTime;
    private Button btnSubmit;
    private LinearLayout llMeetingDate, llStartTime, llEndTime, llBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_meeting);
        if (savedInstanceState == null) {
            initializeView();
        }
    }

    private void initializeView() {

        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE);
        appointmentList = (ArrayList<Appointment>) bundle.get(Constants.INTENT_APPOINTMENT);
        selectedDate = ((Calendar) bundle.getSerializable(Constants.BUNDLE_SELECTED_DATE));
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_DEFAULT_FORMAT);
        tvStartTime = findViewById(R.id.tv_start_time);
        selectedStartTimeString = tvStartTime.getText().toString();
        tvEndTime = findViewById(R.id.tv_end_time);
        selectedEndTimeString = tvEndTime.getText().toString();
        btnSubmit = findViewById(R.id.btn_submit_btn);
        llMeetingDate = findViewById(R.id.ll_meeting_date);
        llStartTime = findViewById(R.id.ll_start_time);
        llEndTime = findViewById(R.id.ll_end_time);
        llBack = findViewById(R.id.ll_back);



        tvDate = findViewById(R.id.tv_date);
        tvDate.setText(selectedDate.get(Calendar.DAY_OF_MONTH) + "/" + (selectedDate.get(Calendar.MONTH)+1) + "/" + selectedDate.get(Calendar.YEAR));

        //Date should not be selected in this page

//        llMeetingDate.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                showDialog(999);
//                return false;
//            }
//        });

        llStartTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showDialog(888);
                return false;
            }
        });

        llEndTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showDialog(777);
                return false;
            }
        });

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitAppointment();
            }
        });

    }

    private void submitAppointment() {
        if (selectedStartTimeString.equals(getResources().getString(R.string.start_time)) || selectedEndTimeString.equals(getResources().getString(R.string.start_time))) {
            Toast.makeText(ScheduleMeetingActivity.this, Constants.MESSAGE_SELECT_START_END_TIME, Toast.LENGTH_SHORT).show();

        } else if (selectedEndTime.compareTo(selectedStartTime) < 0 || selectedEndTime.compareTo(selectedStartTime) == 0) {
            Toast.makeText(ScheduleMeetingActivity.this, Constants.WRONG_TIME, Toast.LENGTH_SHORT).show();
        } else {

            boolean isAppointmentAvailable = true;

            for (int i = 0; i < appointmentList.size(); i++) {

                Calendar startTime = Calendar.getInstance();
                startTime.setTimeInMillis(appointmentList.get(i).getStart_time());

                Calendar endTime = Calendar.getInstance();
                endTime.setTimeInMillis(appointmentList.get(i).getEnd_time());

                if (selectedStartTime.compareTo(startTime) >= 0 && selectedEndTime.compareTo(endTime) <= 0) {
                    isAppointmentAvailable = false;
                } else if (selectedEndTime.compareTo(endTime) <= 0 && selectedEndTime.compareTo(startTime) > 0) {
                    isAppointmentAvailable = false;
                } else if (selectedStartTime.compareTo(startTime) >= 0 && selectedStartTime.compareTo(endTime) < 0) {
                    isAppointmentAvailable = false;
                } else if (selectedStartTime.compareTo(startTime) <= 0 && selectedEndTime.compareTo(endTime) >= 0) {
                    isAppointmentAvailable = false;
                }

            }
            if (isAppointmentAvailable) {
                Toast.makeText(ScheduleMeetingActivity.this, Constants.SLOT_AVAILABLE, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ScheduleMeetingActivity.this, Constants.SLOT__NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {

            final Calendar c = selectedDate;
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(this, myDateListener, year, month, day);

        }

        if (id == 888) {


            int mHour;
            int mMinute;
            if (selectedEndTime != null) {
                mHour = selectedStartTime.getTime().getHours();
                mMinute = selectedStartTime.getTime().getMinutes();
            } else {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
            }

            TimePickerDialog timePickerDialog = new TimePickerDialog(ScheduleMeetingActivity.this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            String am_pm;
                            if (hourOfDay >= 12) {
                                am_pm = "PM";
                            } else {
                                am_pm = "AM";
                            }

                            DecimalFormat formatter = new DecimalFormat("00");
                            selectedStartTimeString = formatter.format(hourOfDay) + ":" + formatter.format(minute) + " " + am_pm;
                            tvStartTime.setText(selectedStartTimeString);
                            selectedStartTime = Calendar.getInstance();
                            selectedStartTime.setTime(selectedDate.getTime());
                            selectedStartTime.clear(Calendar.SECOND);
                            selectedStartTime.clear(Calendar.MILLISECOND);
                            selectedStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            selectedStartTime.set(Calendar.MINUTE, minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();

        }

        if (id == 777) {
            int mHour;
            int mMinute;
            if (selectedEndTime != null) {
                mHour = selectedEndTime.getTime().getHours();
                mMinute = selectedEndTime.getTime().getMinutes();
            } else {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
            }

            TimePickerDialog timePickerDialog = new TimePickerDialog(ScheduleMeetingActivity.this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            String am_pm;
                            if (hourOfDay >= 12) {
                                am_pm = "PM";
                            } else {
                                am_pm = "AM";
                            }

                            DecimalFormat formatter = new DecimalFormat("00");

                            selectedEndTimeString = formatter.format(hourOfDay) + ":" + formatter.format(minute) + " " + am_pm;
                            tvEndTime.setText(selectedEndTimeString);

                            selectedEndTime = Calendar.getInstance();
                            selectedEndTime.setTime(selectedDate.getTime());
                            selectedEndTime.clear(Calendar.SECOND);
                            selectedEndTime.clear(Calendar.MILLISECOND);
                            selectedEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            selectedEndTime.set(Calendar.MINUTE, minute);

                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            selectedDate.set(Calendar.DAY_OF_MONTH, day);
            selectedDate.set(Calendar.MONTH, month);
            selectedDate.set(Calendar.YEAR, year);
            tvDate.setText(selectedDate.get(Calendar.DAY_OF_MONTH) + "/" + (selectedDate.get(Calendar.MONTH)+1) + "/" + selectedDate.get(Calendar.YEAR));
            }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.APPOINTMENT_START_TIME, selectedStartTime);
        outState.putSerializable(Constants.APPOINTMENT_END_TIME, selectedEndTime);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        selectedStartTime = (Calendar) savedInstanceState.getSerializable(Constants.APPOINTMENT_START_TIME);
        selectedEndTime = (Calendar) savedInstanceState.getSerializable(Constants.APPOINTMENT_END_TIME);
        initializeView();

        if (selectedStartTime != null) {
            int mHour = selectedStartTime.getTime().getHours();
            int mMinute = selectedStartTime.getTime().getMinutes();
            String am_pm;
            if (mHour >= 12) {
                am_pm = "PM";
            } else {
                am_pm = "AM";
            }
            DecimalFormat formatter = new DecimalFormat("00");
            selectedStartTimeString = formatter.format(mHour) + ":" + formatter.format(mMinute) + " " + am_pm;
            tvStartTime.setText(selectedStartTimeString);
        }

        if (selectedEndTime != null) {
            int mHour = selectedEndTime.getTime().getHours();
            int mMinute = selectedEndTime.getTime().getMinutes();
            String am_pm;
            if (mHour >= 12) {
                am_pm = "PM";
            } else {
                am_pm = "AM";
            }
            DecimalFormat formatter = new DecimalFormat("00");
            selectedEndTimeString = formatter.format(mHour) + ":" + formatter.format(mMinute) + " " + am_pm;
            tvEndTime.setText(selectedEndTimeString);
        }

    }

}
