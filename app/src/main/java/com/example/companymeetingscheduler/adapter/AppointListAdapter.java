package com.example.companymeetingscheduler.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.companymeetingscheduler.R;
import com.example.companymeetingscheduler.model.Appointment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AppointListAdapter extends RecyclerView.Adapter<AppointListAdapter.MyViewHolder> {
    private ArrayList<Appointment> mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvStartTime, tvEndTime;
        public TextView tvDesc, tvAttendies;

        public MyViewHolder(View v) {
            super(v);
            tvStartTime = (TextView) v.findViewById(R.id.tv_start_time);
            tvEndTime = (TextView) v.findViewById(R.id.tv_end_time);
            tvDesc = (TextView) v.findViewById(R.id.tv_desc);
            tvAttendies = (TextView) v.findViewById(R.id.tv_attendies);
        }
    }

    public AppointListAdapter(ArrayList<Appointment> myDataset) {
        mDataset = myDataset;
    }


    @Override
    public AppointListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_data_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Appointment appointment = mDataset.get(position);
        TextView tvStartTime = holder.tvStartTime;
        TextView tvEndTime = holder.tvEndTime;

        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        String startTime = formatter.format(new Date(appointment.getStart_time()));
        String endTime = formatter.format(new Date(appointment.getEnd_time()));

        tvStartTime.setText(startTime);
        tvEndTime.setText(endTime);


        TextView tvDesc = holder.tvDesc;
        tvDesc.setText(appointment.getDescription());

        TextView tvAttendies = holder.tvAttendies;
        if(tvAttendies!=null){
            String attendies = "";
            for(String attendie : appointment.getParticipants()){
                if(attendies == ""){
                    attendies = attendie;
                } else {
                    attendies = attendies + ", "+ attendie;
                }

            }
           tvAttendies.setText(attendies);

        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}